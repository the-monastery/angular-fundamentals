var eventsApp;

(function(){
    'use strict';

    var routeDefinitions = [
        ['/newEvent', 'templates/NewEvent.html', 'EditEventController', 'B'],
        ['/editProfile', 'templates/EditProfile.html', 'EditProfileController', 'C'],
        ['/eventList', 'templates/EventList.html', 'EventListController', 'D'],
        ['/cacheExample', 'templates/CacheExample.html', 'CacheController', 'E'],
        ['/compileExample', 'templates/CompileExample.html', 'CompileServiceController', 'F'],
        ['/cookieStoreExample', 'templates/CookieStoreExample.html', 'CookieStoreController', 'G'],
        ['/filterExample', 'templates/FilterExample.html', 'FilterController', 'H'],
        ['/localeExample', 'templates/LocaleExample.html', 'LocaleController', 'I'],
        ['/directiveExamples', 'templates/DirectiveExamples.html', null, null],
        ['/timeoutExample', 'templates/TimeoutServiceExample.html', 'TimeoutServiceController', 'J']
    ];

    eventsApp = angular.module('eventsApp', ['ngSanitize', 'ngResource', 'ngCookies', 'ngRoute'])
        .factory('eventCache', function($cacheFactory) {
            return $cacheFactory('eventCache', {capacity:3});
        });



    eventsApp.config(function($routeProvider, $locationProvider){

        var addRoute = function(routeDefinition){

            $routeProvider.when(routeDefinition[0], {

                templateUrl:routeDefinition[1],
                controller:routeDefinition[2],
                foo:routeDefinition[3]

            });

        };

        //Add each route from above
        routeDefinitions.forEach(addRoute);

        //This is a specific implementation that uses the resolve parameter.
        $routeProvider.when('/event/:eventId', {

            templateUrl: 'templates/EventDetails.html',

            controller: 'EventController',

            //resolve is useful for pausing the render of a page when async data is loading
            resolve: {
                event : function($route, eventData){
                    //whenever you return a promise, angular will pause before rendering the entire page
                    return eventData.getEvent($route.current.pathParams.eventId).$promise;
                }
            }

        });

        $routeProvider.when('/routeTemplate', {
            template: 'I am the result of calling route template.'
        });

        $routeProvider.otherwise({redirectTo:"/eventList"});

        $locationProvider.html5Mode(true);

    });

})();
