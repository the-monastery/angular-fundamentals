/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.controller('MainMenuController', function MainMenuController($scope, $location) {

        console.log('absUrl:', $location.absUrl());
        console.log('protocol:', $location.protocol());
        console.log('port:', $location.port());
        console.log('host:', $location.host());
        console.log('path:', $location.path());
        console.log('search:', $location.search());
        console.log('hash:', $location.hash());
        console.log('url:', $location.url());

        $scope.navigate = function(path) {
            //replace the page in history with the current view
            //$location.replace();
            $location.url(path);
        };
    });

})();