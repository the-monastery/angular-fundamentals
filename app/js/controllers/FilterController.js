/**
 * Created by nick on 2015-03-08.
 */
(function(){
    'use strict';

    eventsApp.controller('FilterController', function FilterController($scope, durationsFilter) {
    //This method is a less desirable way retrieve a filter... above is clearly superior
    //eventsApp.controller('FilterController', function FilterController($scope, $filter) {

        $scope.data = {};

        /*var durations = $filter("durations");
        $scope.data.duration1 = durations(1);
        $scope.data.duration2 = durations(2);
        $scope.data.duration3 = durations(3);
        $scope.data.duration4 = durations(4);*/

        $scope.data.duration1 = durationsFilter(1);
        $scope.data.duration2 = durationsFilter(2);
        $scope.data.duration3 = durationsFilter(3);
        $scope.data.duration4 = durationsFilter(4);

    });

})();