/**
 * Created by nick on 2015-03-07.
 */
(function(){
    'use strict';

    eventsApp.controller('CacheController', function CacheController($scope, eventCache) {

        $scope.addToCache = function(key, value) {
            eventCache.put(key, value);
        };

        $scope.readFromCache = function(key) {
            return eventCache.get(key);
        };

        $scope.getCacheStats = function() {
            return eventCache.info();
        };

    });

})();