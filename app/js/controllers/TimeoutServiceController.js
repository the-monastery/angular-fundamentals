/**
 * Created by nick on 2015-03-07.
 */
(function(){
    'use strict';

    eventsApp.controller('TimeoutServiceController', function TimeoutServiceController($scope, $timeout) {

        var promise = $timeout(function(){

            $scope.name = "Nicholas Hillier";
            throw {message:"NINJA STYLE"};

        }, 3000);

        $scope.cancel = function(){

            $timeout.cancel(promise);

        };

    });

})();