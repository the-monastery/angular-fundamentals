(function(){
    'use strict';

    eventsApp.controller('EventController', function EventController($scope, $anchorScroll, $routeParams, $route){

        $scope.sortOrder = 'name';
        $scope.snippet = '<span style="color:red">hi there</span>';
        $scope.emphasize = {color:'yellow'};
        $scope.special = 'special';
        $scope.buttonDisabled = false;
        $scope.message = "red";

        //Apparently event is available from the route properties off of the resolve object.
        //Not completely clear how this works
        $scope.event = $route.current.locals.event; //eventData.getEvent($routeParams.eventId);

        /*
        //The promise can be retrieved from the resource.
        $scope.getEvent().$promise.then(
            function(event) {
                $scope.event = event;
                console.log(event);
            },
            function(event) {
                console.log(event);
            }
        );*/

        $scope.onButtonClick = function() {
            $scope.emphasize = {color:$scope.message};
        };

        $scope.upVoteSession = function(session){
            session.upVoteCount++;
        };

        $scope.downVoteSession = function(session){
            session.upVoteCount--;
        };

        $scope.scrollToSession = function(){
            $anchorScroll();
        }

    });

})();

/*
 //When using $http an $q services, you have to use the promise pattern
 eventData.getEvent().then(
 function(event){$scope.event = event;},
 function(statusCode) {console.log(statusCode);}
 );
 */
