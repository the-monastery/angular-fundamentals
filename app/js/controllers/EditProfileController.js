(function(){
    'use strict';

    eventsApp.controller('EditProfileController', function EditProfileController($scope, userData, utils) {

        $scope.user = {};

        $scope.getGravatarUrl = function(email){

            var gravatarUrl = "http://www.gravatar.com/avatar/";

            if(!utils.isValidEmail(email)) {
                return gravatarUrl + "000?s=200";
            }

            return gravatarUrl + utils.MD5(email) + ".jpg?s=200&r=g";
        };

        $scope.saveUser = function(user){
            userData.saveUser(user);
        }

    });

})();