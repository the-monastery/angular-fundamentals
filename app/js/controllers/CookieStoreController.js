/**
 * Created by nick on 2015-03-08.
 */
(function(){
    'use strict';

    eventsApp.controller('CookieStoreController', function CookieStoreController($scope, $cookieStore) {

        //!!IMPORTANT... it is not possible to specify an expiration date for a cookie
        $scope.thisEvent = {id:1, name:"Hockey School"};

        $scope.saveEventToCookie = function(event){
            $cookieStore.put("event", event);
        };

        $scope.getEventFromCookie = function(){
            console.log($cookieStore.get("event"));
        };

        $scope.removeFromCookie = function(){
            $cookieStore.remove("event");
        }

    });

})();