/**
 * Created by nick on 2015-03-07.
 */
(function(){
    'use strict';

    eventsApp.controller('CompileServiceController', function CompileServiceController($scope, $compile, $parse, $route) {

        var fn = $parse('1 + 2');
        console.log(fn());
        console.log($route.current.foo);
        console.log($route.current.params.foo);
        console.log($route.current.params.bar);

        var getter = $parse("event.name");

        var context1 = {event:{name:"Nicholas"}};
        var context2 = {event:{name:"Logan"}};

        console.log(getter(context1));
        console.log(getter(context2));

        console.log(getter(context1, context2));

        var assign = getter.assign;
        assign(context1, "Tracey");
        console.log(getter(context1));

        // This throws an isecdom error... you cannot access dom
        // elements in an expression because it is hard to proteck
        // code in this manner
        // https://docs.angularjs.org/error/$parse/isecdom
        $scope.appendToDivElement = function(markup){
            var compiledMarkup = $compile(markup);
            var scopeResult = compiledMarkup($scope);
            var appendElement = angular.element("#appendHere");
            return scopeResult.appendTo(appendElement);
        };

        $scope.reload = function(){
            $route.reload();
        }

    });

})();