/**
 * Created by nick on 2015-03-07.
 */
(function(){
    'use strict';

    eventsApp.controller('LocaleController', function LocaleController($scope, $locale) {

        $scope.eventDate = Date.now();
        $scope.dateFormat = $locale.DATETIME_FORMATS.fullDate;

    });

})();