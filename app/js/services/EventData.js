(function(){
    'use strict';

    eventsApp.factory('eventData', function($resource) {

        var resource = $resource('data/event/:id', {id:'@id'});
        var allEvents = [];

        var eventData = {
            getEvent : function(id) {
                return resource.get({id:id});
            },
            saveEvent : function(event){
                event.id = allEvents.length + 1;
                return resource.save(event);
            },
            getAllEvents:function(){
                return allEvents;
            }
        };

        var getNextEvent = function(startIndex, completeCallback){
            eventData.getEvent(startIndex).$promise.then(
                function success(event){
                    allEvents[startIndex-1] = event;
                    getNextEvent(++startIndex, completeCallback);
                },
                function fail(){
                    if(completeCallback) {
                        completeCallback(allEvents);
                    }
                }
            )
        };

        getNextEvent(1);

        return eventData;

    });

})();


/*
//The following example shows how to us the $q deferred promise and the $http services.
 var deferred = $q.defer();

 $http({method:'GET', url:'data/event/1'}).
 success(function(data, status, headers, config){
 deferred.resolve(data);
 }).
 error(function(data, status, headers, config){
 deferred.reject(status);
 });

 return deferred.promise;*/

