/**
 * Created by nick on 2015-03-07.
 */
(function(){
    'use strict';

    eventsApp.factory('$exceptionHandler', function () {
        return function(ex){
            console.log("Champion handling of all exceptions " + ex.message);
        };
    });
    
})();