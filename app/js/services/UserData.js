/**
 * Created by nick on 2015-03-08.
 */
(function(){
    'use strict';

    eventsApp.factory('userData', function($resource) {

        var resource = $resource('data/user/:name', {name:'@name'});

        var userData = {
            loginUser: function(name){
                return resource.get(name);
            },
            saveUser: function(user){
                return resource.save(user);
            }
        };

        return userData;
    });

})();