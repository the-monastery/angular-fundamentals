/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('simpleDirective', function simpleDirective() {
        var output = {
            template:'<input type="text" ng-model="simpleData" /> <br/> {{simpleData}}',
            scope:{}
        };
        return output;
    })

})();