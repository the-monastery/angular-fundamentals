/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';
    eventsApp.directive('compiledDirective', function compiledDirective($compile) {

        var output = {

            restrict: 'E',//E(element), A(attribute), C(class), M(comment)

            link: function (scope, element, attrs, controller) {
                var markup = '<input type="text" ng-model="sampleData" /> {{sampleData}} <br/>';
                angular.element(element).html($compile(markup)(scope));
            }

        };

        return output;

    })
})();