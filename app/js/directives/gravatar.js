/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('gravatar', function gravatar(utils) {

        var output = {

            template: '<img />',

            replace: true,

            restrict: 'E',

            link: function(scope, element, attrs, controller){

                attrs.$observe('email', function(newVal, oldVal){

                    if(newVal != oldVal) {
                        attrs.$set('src', utils.getGravatarAvatar(newVal) );
                    }

                });
            }

        };

        return output;

    });


})();