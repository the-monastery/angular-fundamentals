/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('dateKeys', function dateKeys() {

        var output = {

            restrict:'A',

            link: function (scope, element, attrs, controller) {
                element.on('keydown', function(event){
                    return isValidKeyCode(event.keyCode);
                });
            }

        };

        return output;

    });

    function isValidKeyCode(code)
    {
        return isNumericKey(code) || isForwardSlash(code) || isNavigationKeycode(code);
    }

    function isNumericKey(keyCode){
        return (keyCode >= 48 && keyCode <=57)
            || (keyCode >= 96 && keyCode <=105);
    }

    function isForwardSlash(keyCode){
        return keyCode === 191;
    }

    function isNavigationKeycode(keyCode){
        return [
            8, //backspace
            35, //end
            36, //home
            37, //left
            38, //up
            39, //right
            40, //down
            45, //ins
            46 //del
        ].indexOf(keyCode) >= 0;
    }


})();