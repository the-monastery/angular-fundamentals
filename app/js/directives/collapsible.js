/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('collapsible', function collapsible() {

        var output = {

            template: '<div>' +
                        '<h4 class="well-title" ng-click="toggleVisibility()">{{title}}</h4>' +
                        '<div ng-transclude ng-show="visible"></div>' +
                      '</div>',
            replace: true,
            transclude:true,
            restrict: 'E',
            scope: {
                title:"@"
            },
            controller:function($scope){
                $scope.visible = true;
                $scope.toggleVisibility = function(){
                    $scope.visible = !$scope.visible;
                };
            }

        };

        return output;

    });


})();