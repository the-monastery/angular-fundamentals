/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('greetings', function greetings($compile) {

        var output = {
            replace: true,
            restrict: 'E',
            template: '<button class="btn" ng-click="sayHello()">Say Hello</button>',
            controller: greetingsController//'@', can specifiy the controller directly with a function, but the controller lookup name, or as stated here.
            //name:'controller'

        };

        return output;

    })
    .directive('finnish', function finnish() {

        return {
            restrict: 'A',
            require:'greetings',
            link: function (scope, element, attrs, controller) {
                controller.addGreeting("Hei");
            }

        };

    })
    .directive('french', function french() {

        return {
            restrict: 'A',
            require:'greetings',
            link: function (scope, element, attrs, controller) {
                controller.addGreeting("Bonjour");
            }

        };

    });

    var greetingsController = function GreetingsController($scope) {

        var greetings = ["Hello"];

        $scope.sayHello = function(){
            alert(greetings.join());
        };

        this.addGreeting = function(greeting){
            greetings.push(greeting);
        }

    };


})();