/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('repeatX', function repeatX() {

        var output = {
            //this is an example of an expensive version of compiling, because the entire dom is traversed.
            //link:function(scope, element, attributes, controller){
            compile:function(element, attributes){
                for(var i = 1; i < Number(attributes.repeatX); i++){
                    //scope is not needed, as the template of the element is simply cloned.
                    //element.after($compile(element.clone().attr('repeat-x', 0))(scope));
                    element.after(element.clone().attr('repeat-x', 0));
                }
                return function(scope, element, attributes, controller){
                    attributes.$observe('text', function(newValue){
                        if(newValue === 'Hello World') {
                            element.css('color', 'red');
                        }
                    });
                }
            }
        };

        return output;

    });


})();