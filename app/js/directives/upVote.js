/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('upVote', function upVote() {

        var output = {

            templateUrl: 'templates/directives/upVote.html',

            restrict: 'E',

            scope: {
                upvote: "&",
                downvote: "&",
                count: "="
            }

        };

        return output;

    });


})();