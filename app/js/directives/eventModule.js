/**
 * Created by nick on 2015-03-15.
 */
(function(){
    'use strict';

    eventsApp.directive('eventModule', function eventModule() {

        var output = {

            templateUrl: 'templates/directives/EventModule.html',

            replace: true, //replace the defining element

            restrict: 'E', //E(element), A(attribute), C(class), M(comment)

            scope: {model:"=model"}

        };

        return output;

    })

})();