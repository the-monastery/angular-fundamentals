/**
 * Created by nick on 2015-03-23.
 */
(function(){
    'use strict';

    app.controller('BooksController', BooksController);
    BooksController.$inject = ['books', 'dataService', 'badgeService', 'logger'];

    function BooksController(books, dataService, badgeService, logger) {

        var vm = this;

        vm.getBadge = badgeService.getBadge;
        vm.appName = books.appName;
        vm.appDescription = books.appDescription;

        dataService.getAllBooks()
            .then(booksSuccess, null, booksNotify)
            .catch(booksFail)
            .finally(booksFinally);

        dataService.getAllReaders()
            .then(readersSuccess)
            .catch(readersFail)
            .finally(readersFinally);

        function booksSuccess(books){
            vm.allBooks = books;
            books.forEach(logger.logBook);
        }

        function booksNotify(message) {
            logger.output(message);
        }

        function booksFail(reason){
            logger.output(reason);
        }

        function booksFinally(){
            logger.output("books complete");
        }

        function readersSuccess(readers){
            vm.allReaders = readers;
        }

        function readersFail(reason){
            logger.output(reason);
        }

        function readersFinally(){
            logger.output("readers complete");
        }
    }
})();