/**
 * Created by nick on 2015-03-23.
 */

var app;

(function(){
    'use strict';

    app = angular.module('app', []);

    app.config(appConfig);
    appConfig.$inject = ['booksProvider', 'appConstants'];

    function appConfig(booksProvider, appConstants) {

        booksProvider.setIncludeVersionInTitle(true);
        console.log(appConstants.APP_NAME + " " + appConstants.APP_VERSION);

    }

})();