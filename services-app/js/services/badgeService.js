/**
 * Created by nick on 2015-03-23.
 */
(function(){
    'use strict';

    app.value('badgeService', {
        getBadge:badgeService
    });

    function badgeService(readingMinutes){

        var output;

        switch(true){
            case (readingMinutes <= 200):
                output = "Beginner";
                break;
            case (readingMinutes <= 500):
                output = "Intermediate";
                break;
            case (readingMinutes > 500):
                output = "Advanced";
                break;
            default :
                output = "N/A";
        }

        return output;
    }

})();