/**
 * Created by nick on 2015-03-23.
 */
(function(){
    'use strict';

    app.constant('appConstants', {
        APP_VERSION:"1.0",
        APP_DESCRIPTION:"This is a demo app",
        APP_NAME:"Ghostmonk Demo Angular App"
    });

})();