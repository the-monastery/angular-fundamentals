/**
 * Created by nick on 2015-03-23.
 */
(function(){
    'use strict';

    app.factory('dataService', dataService);
    dataService.$inject = ['$q', '$timeout'];

    function dataService($q, $timeout){

        var deferredBooks = $q.defer();
        var deferredReaders = $q.defer();

        $timeout(sendBooks, 1000);
        $timeout(sendReaders, 1500);

        function getAllBooks(){
            return deferredBooks.promise;
        }

        function getAllReaders(){
            return deferredReaders.promise;
        }

        function sendBooks(){
            var success = true;

            if(success){
                deferredBooks.notify("Progress report 1.");
                deferredBooks.notify("Progress report 2.");
                deferredBooks.resolve(allBooks);
                deferredBooks.notify("Progress report 3.");
            }
            else{
                deferredBooks.reject("The lawnmower man has found you.");
            }
        }

        function sendReaders(){
            deferredReaders.resolve(allReaders);
        }

        return {
            getAllBooks:getAllBooks,
            getAllReaders:getAllReaders
        };

    }

    var allBooks = [
        {
            bookId:1,
            title:"Primal Leadership",
            author:"Daniel Goleman",
            yearPublished:2002
        },
        {
            bookId:2,
            title:"Moveable Feast",
            author:"Earnest Hemmingway",
            yearPublished:1964
        },
        {
            bookId:3,
            title:"The Naked and the Dead",
            author:"Norman Mailer",
            yearPublished:1948
        }
    ];

    var allReaders = [
        {
            readerId:1,
            name:"Nicholas Hillier",
            weeklyReadingGoal:300,
            totalMinutesRead:1000
        },
        {
            readerId:2,
            name:"Lucas Hillier-Huot",
            weeklyReadingGoal:150,
            totalMinutesRead:500
        },
        {
            readerId:3,
            name:"Logan Hillier-Huot",
            weeklyReadingGoal:100,
            totalMinutesRead:200
        }
    ];

})();