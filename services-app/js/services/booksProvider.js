/**
 * Created by nick on 2015-03-23.
 */
(function(){
    'use strict';

    app.provider('books', booksProvider);
    booksProvider.$inject = ['appConstants'];

    function booksProvider(appConstants){
        this.includeVersionInTitle = false;
        this.setIncludeVersionInTitle = function(value){
            this.includeVersionInTitle = value;
        };

        this.$get = function(){
            var appName = appConstants.APP_NAME;
            var appDescription = appConstants.APP_DESCRIPTION;

            var version = appConstants.APP_VERSION;

            if(this.includeVersionInTitle){
                appName = appName + " " + version;
            }

            return {
                appName:appName,
                appDescription:appDescription
            };
        };
    }

})();